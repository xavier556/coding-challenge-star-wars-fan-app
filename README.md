# Coding Challenge - Star Wars Fan App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Introduction

### Bootstrap
The components views are based on bootstrap 4 components and principles. It is a mobile-first approach, most of the responsive behaviour is built in bootstrap, the custom responsive css for the application structure can be found in frame.component.scss.

### Login
The login functionality uses the browser local storage, meaning a user stays logged in even if the browser is closed.

### Cache
There is a simple mechanic of url caching using the local storage (see swapi.service.ts). This means a query to the swapi.co api will only send an http request the first time it is called. The cache can be cleared from the application menu or by clearing the browser local storage.

## Project structure
```
|-- src
  |-- app
    |-- components
    |-- frame                        // main application component
        |-- frame.component.ts       // contains the header, footer, side menu and router outlet
        |-- frame.component.scss     // where most of the responsive stuff happens
      |-- pages                      // 'full page' components, usually mapped to an angular route
        |-- ...
      |-- ui                         // re-usable ui components like search bar, item list, etc...
        |-- ...
    |-- services
      |-- auth          
        |-- auth.service.ts          // authentication service using local storage for simplicity
        |-- auth-guard.service.ts    // simple angular routing guard
      |-- data.service.ts            // service to get (typed) resources and items
      |-- swapi.service.ts           // wrapper to the swapi.co api, include simple cache mechanics
    |-- types
      |-- resource.interface.ts      // a typescript interface for resources returned by the swapi.co api
      |-- item.interface.ts          // a typescript interface for items returned by the swapi.co api
      |-- resource.enum.ts           // an enumeration of the six resources provided by swapi.co
    |-- app-routing.module.ts        // define all application routes
  |-- assets
    |-- css                          // some custom non-functional css
      |-- animated-stars.scss        // backround animated stars with a parallax effect
      |-- menu-toggle.scss           // animate the menu toggle button when clicked
      |-- open-iconic-bootstrap.css  // various icons
      |-- opening-crawl.scss         // 3d animation to look like a Star Wars movie opening
    |-- fonts 
    |-- images 
  |-- style.scss                     // general custom css rules
```

## Credits

The images in the assets folder are free of use and come from https://flaticon.com.

The StarWars-like webfont is called 'Distant Galaxy' and was generated from https://www.fontsquirrel.com.

The animated background is widely inspired by https://codepen.io/saransh/pen/BKJun.
