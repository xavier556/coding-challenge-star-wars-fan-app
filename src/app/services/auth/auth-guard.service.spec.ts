import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './auth.service';
import { LoginComponent } from 'src/app/components/pages/login/login.component';
import { FormsModule } from '@angular/forms';
import { HomeComponent } from 'src/app/components/pages/home/home.component';

describe('AuthGuardService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			HttpClientTestingModule,
			RouterTestingModule.withRoutes([
				{ path: 'login', component: LoginComponent },
				{ path: 'home', component: HomeComponent }
			]),
			FormsModule
		],
		declarations: [
			LoginComponent,
			HomeComponent
		]
	}));

	it('should be created', () => {
		const service: AuthGuardService = TestBed.get(AuthGuardService);
		expect(service).toBeTruthy();
	});

	it('should allow navigation if logged in', () => {
		const service: AuthGuardService = TestBed.get(AuthGuardService);
		const auth: AuthService = TestBed.get(AuthService);
		auth.login('fake_username');
		expect(service.canActivate()).toBeTruthy();
	});

	it('should not allow navigation if not logged in', () => {
		const service: AuthGuardService = TestBed.get(AuthGuardService);
		const auth: AuthService = TestBed.get(AuthService);
		auth.logout();
		expect(service.canActivate()).toBeFalsy();
	});
});
