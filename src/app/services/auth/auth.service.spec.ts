import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { AuthService } from './auth.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HomeComponent } from 'src/app/components/pages/home/home.component';
import { LoginComponent } from 'src/app/components/pages/login/login.component';
import { FormsModule } from '@angular/forms';

describe('AuthService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		imports: [
			HttpClientTestingModule,
			RouterTestingModule.withRoutes([
				{ path: 'home', component: HomeComponent },
				{ path: 'login', component: LoginComponent }
			]),
			FormsModule
		],
		declarations: [
			HomeComponent,
			LoginComponent
		]
	}));

	it('should be created', () => {
		const service: AuthService = TestBed.get(AuthService);
		expect(service).toBeTruthy();
	});

	it('should be logged after login', () => {
		const service: AuthService = TestBed.get(AuthService);
		service.login('fake_username');
		expect(service.isAuthenticated).toBeTruthy();
	});

	it('should not be logged after logout', () => {
		const service: AuthService = TestBed.get(AuthService);
		service.logout();
		expect(service.isAuthenticated()).toBeFalsy();
	});
});
