import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { EResource } from '../types/resource.enum';

const BASE_URL = "https://swapi.co/api/";
const CACHE_PREFIX: string = 'swapi-cache-';

@Injectable({
	providedIn: 'root'
})
export class SwapiService {

	/**
	 * Constructor
	 * @param http Angular http service.
	 */
	constructor(private http: HttpClient) { }

	/**
	 * Query to get the list of all resources.
	 * BASE_URL
	 */
	getResources(): Observable<any> {
		return this.getUrl(BASE_URL);
	}

	/**
	 * Query to search a resource for a term.
	 * BASE_URL/:resource/?search=:term
	 * @param resource 
	 * @param term 
	 */
	searchResource(resource: EResource, term: string) {
		const url = `${BASE_URL + resource}/?search=${term}`;
		return this.getUrl(url);
	}

	/**
	 * Query to get one page of a resource.
	 * BASE_URL/:resource/?page=:page
	 * @param resource 
	 * @param page 
	 */
	getResource(resource: EResource, page: number) {
		const pageArg: string = (page && page > 1) ? '/?page=' + page : '';
		const url = BASE_URL + resource + pageArg;
		return this.getUrl(url);
	}

	/**
	 * Query to get on item.
	 * BASE_URL/:resource/:id
	 * @param resource 
	 * @param id 
	 */
	getItem(resource: EResource, id: string) {
		const url = `${BASE_URL + resource}/${id}`;
		return this.getUrl(url);
	}

	/**
	 * Sends a request to the provided url and cache the result.
	 * @param url 
	 * @returns {Observable<any>}
	 */
	private getUrl(url: string): Observable<any> {

		// Try to get the data from the local cache.
		const cachedUrl: string = CACHE_PREFIX + url;
		const cached = localStorage.getItem(cachedUrl);

		if (cached) {
			// If the query is cached return an observable of the data.
			return of(JSON.parse(cached));
		}
		else {
			// If not cached send a request to get the data.
			// The pipe is used to cache the query.
			return this.http.get(url).pipe(map(res => {
				localStorage.setItem(cachedUrl, JSON.stringify(res))
				return res;
			}));
		}
	}

	/**
	 * Clear the cache.
	 */
	public clearCache(): number {

		// First gather all the keys to be deleted. They start
		// with CACHE_PREFIX.
		let arr = [];
		let counter: number = 0;
		for (let i = 0; i < localStorage.length; i++) {
			const key = localStorage.key(i);
			if (key.startsWith(CACHE_PREFIX)) {
				arr.push(key);
			}
		}

		// Delete those entries from the local cache.
		arr.forEach(key => {
			console.log("removing entry " + key);
			localStorage.removeItem(key);
			counter++
		});

		// Return the number of deleted entries.
		return counter;
	}

	/**
	 * Return an object with all the cached key/values.
	 */
	public getCache() {
		const obj = {};
		for (let i = 0; i < localStorage.length; i++) {
			const key = localStorage.key(i);
			if (key.startsWith(CACHE_PREFIX)) {
				obj[key] = localStorage[key];
			}
		}
		return obj;
	}

	/**
	 * Load the data from assets/cache/cache.json in cache.
	 */
	public getCacheSample() {
		return this.http.get('assets/cache/cache.json');
	}
}
