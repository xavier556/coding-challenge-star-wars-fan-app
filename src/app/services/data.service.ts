import { Injectable } from '@angular/core';
import { SwapiService } from './swapi.service';
import { forkJoin, Observable, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { IItem } from '../types/item.interface';
import { IResource } from '../types/resource.interface';
import { EResource } from '../types/resource.enum';


@Injectable({
	providedIn: 'root'
})
export class DataService {

	/**
	 * Constructor
	 * @param {SwapiService} swapi Starwars api service.
	 */
	constructor(private swapi: SwapiService) { }

	/**
	 * Get an observable of the list of all resources.
	 */
	getResources(): Observable<any> {
		return this.swapi.getResources();
	}

	/**
	 * Get an observable of a resource (like people, films, etc...). Resources
	 * are paged by 10 items.
	 * @param {string} resource The name of the resource.
	 * @param {number} page The page number.
	 * @returns {Observable<IResource>}
	 */
	getResource(resource: EResource, page: number = 1): Observable<IResource> {
		return this.swapi.getResource(resource, page).pipe(map((res: IResource) => {
			res.resourcePage = Number(page);
			res.resourceName = resource;
			return res;
		}));
	}

	/**
	 * Get an observable of an item (like a people, a film, etc...).
	 * @param {string} resource The resource name.
	 * @param {string} id The item id.
	 * @returns {Observable<IItem>}
	 */
	getItem(resource: EResource, id: string): Observable<IItem> {
		return this.swapi.getItem(resource, id).pipe(map((res: IItem) => {
			res.itemName = res.name || res.title;
			res.itemResource = resource;
			res.itemId = id;
			return res;
		}));
	}

	/**
	 * Search for a term in one specific resource. Returns an observable
	 * of a resource.
	 * @param {EResource} resource 
	 * @param {string} term 
	 */
	searchResource(resource: EResource, term: string): Observable<IResource> {
		return this.swapi.searchResource(resource, term).pipe(map(res => {
			res.resourceName = resource;
			return res;
		}));
	}

	/**
	 * Search for a term in all resources. Returns an observable of an 
	 * array of resources.
	 * @param {string} term 
	 */
	searchTerm(term: string): Observable<Array<IResource>> {

		// We have to query each resource with the given term.
		// Creates an array with one observable for each resource.
		const batch: Array<Observable<IResource>> = [
			this.searchResource(EResource.people, term),
			this.searchResource(EResource.films, term),
			this.searchResource(EResource.starships, term),
			this.searchResource(EResource.vehicles, term),
			this.searchResource(EResource.planets, term),
			this.searchResource(EResource.species, term)
		];

		// Use forkJoin to get an array of all those observable values.
		// The pipe is used to remove resources with no results.
		return forkJoin(batch).pipe(map(res => res.filter(r => r.count > 0)));
	}

}
