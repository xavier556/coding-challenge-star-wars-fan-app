import { TestBed } from '@angular/core/testing';

import { SwapiService } from './swapi.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('SwapiService', () => {
	beforeEach(() => TestBed.configureTestingModule({
		imports: [HttpClientTestingModule]
	}));

	it('should be created', () => {
		const service: SwapiService = TestBed.get(SwapiService);
		expect(service).toBeTruthy();
	});
});
