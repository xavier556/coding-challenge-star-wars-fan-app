import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router, RouterEvent } from '@angular/router';
import { SwapiService } from 'src/app/services/swapi.service';
import { filter } from 'rxjs/operators';

@Component({
	selector: 'app-frame',
	templateUrl: './frame.component.html',
	styleUrls: ['./frame.component.scss']
})
export class FrameComponent implements OnInit {

	// Controls the state of the left side menu.
	menuOpen: boolean = false;

	/**
	 * Constructor.
	 * @param auth 
	 * @param router 
	 */
	constructor(public auth: AuthService, public router: Router) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		// Hook into router events to close the left side menu when navigating
		// to a new route.
		this.router.events.pipe(
			filter(e => e instanceof RouterEvent)
		).subscribe(e => {
			this.menuOpen = false;
		});
	}
}
