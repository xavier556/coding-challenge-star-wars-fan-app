import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameComponent } from './frame.component';
import { HomeComponent } from '../pages/home/home.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from '../ui/search-bar/search-bar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NavBarComponent } from '../ui/nav-bar/nav-bar.component';

describe('FrameComponent', () => {
	let component: FrameComponent;
	let fixture: ComponentFixture<FrameComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule
			],
			declarations: [
				FrameComponent,
				HomeComponent,
				SearchBarComponent,
				NavBarComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(FrameComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
