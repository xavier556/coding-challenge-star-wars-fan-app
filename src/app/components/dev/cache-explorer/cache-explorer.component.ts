import { Component, OnInit } from '@angular/core';
import { SwapiService } from 'src/app/services/swapi.service';

@Component({
	selector: 'app-cache-explorer',
	templateUrl: './cache-explorer.component.html',
	styleUrls: ['./cache-explorer.component.scss']
})
export class CacheExplorerComponent implements OnInit {

	public cache = {};

	constructor(public swapi: SwapiService) { }

	ngOnInit() {
		this.displayCache();
	}

	public displayCache() {
		this.cache = this.swapi.getCache();
	}

	public clearCache() {
		if (confirm('Clear ' + this.getCacheSize() + ' entries from local cache?') === true) {
			const n = this.swapi.clearCache();
			this.displayCache();
			alert(n + ' entries removed from cache.');
		}
	}

	public loadCache() {
		let counter: number = 0;
		this.swapi.getCacheSample().subscribe(json => {
			for (const key in json) {
				if (!localStorage.getItem(key)) {
					localStorage.setItem(key, json[key]);
					counter++;
				}
			}
			this.displayCache();
			alert(counter + " entries loaded in cache.")
		}, error => {
			alert("Error: " + error);
		});

	}

	/**
	 * Create and download a json file of the cached entries.
	 */
	public downloadCache() {
		const element = document.createElement('a');
		element.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(this.cache)));
		element.setAttribute('download', 'cache.json');
		element.style.display = 'none';
		document.body.appendChild(element);
		element.click();
		document.body.removeChild(element);
	}

	public getCacheSize(): number {
		return Object.keys(this.cache).length;
	}
}
