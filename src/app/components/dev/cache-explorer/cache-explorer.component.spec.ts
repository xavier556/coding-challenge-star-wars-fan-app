import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CacheExplorerComponent } from './cache-explorer.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CacheExplorerComponent', () => {
	let component: CacheExplorerComponent;
	let fixture: ComponentFixture<CacheExplorerComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule
			],
			declarations: [CacheExplorerComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(CacheExplorerComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
