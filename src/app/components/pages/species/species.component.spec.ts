import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeciesComponent } from './species.component';
import { ItemComponent } from '../../ui/item/item.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { ItemListComponent } from '../../ui/item-list/item-list.component';

describe('SpeciesComponent', () => {
	let component: SpeciesComponent;
	let fixture: ComponentFixture<SpeciesComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule
			],
			declarations: [
				SpeciesComponent,
				ItemListComponent,
				ItemComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SpeciesComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
