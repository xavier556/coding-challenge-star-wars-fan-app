import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EResource } from 'src/app/types/resource.enum';
import { IItem } from 'src/app/types/item.interface';

@Component({
	selector: 'app-planet',
	templateUrl: './planet.component.html',
	styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {

	// The model for this component's view.
	public item: IItem = null;

	// A state flag that is set to true when data is available.
	public ready: boolean = false;

	/**
	 * Constructor.
	 * @param route 
	 * @param data 
	 */
	constructor(public route: ActivatedRoute, public data: DataService) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			this.ready = false;
			this.data.getItem(EResource.planets, params.id).subscribe(result => {
				console.log(result);
				this.item = result;
				this.ready = true;
			}, error => {
				this.item = null;
				this.ready = true;
			});
		});
	}
}
