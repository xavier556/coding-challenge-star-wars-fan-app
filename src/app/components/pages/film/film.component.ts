import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { IItem } from 'src/app/types/item.interface';
import { EResource } from 'src/app/types/resource.enum';

@Component({
	selector: 'app-film',
	templateUrl: './film.component.html',
	styleUrls: ['./film.component.scss']
})
export class FilmComponent implements OnInit {

	// The model for this component's view.
	public item: IItem = null;

	// Manage the opening crawl. The crawl array contains
	// one paragraph per item because the api returns a text
	// with \r\n\r\n characters to separate them and it is not
	// convenient to use.
	// The playCrawl boolean add/remove the 'crawl' css class.
	public crawl: Array<string> = [];
	public playCrawl: boolean = false;

	// A state flag that is set to true when data is available.
	ready: boolean = false;

	/**
	 * Constructor
	 * @param route 
	 * @param data 
	 */
	constructor(public route: ActivatedRoute, public data: DataService) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params.id) {
				this.ready = false;
				this.data.getItem(EResource.films, params.id).subscribe(res => {
					this.item = res;
					this.crawl = this.item.opening_crawl.split('\r\n\r\n');
					this.ready = true;
				}, error => {
					this.item = null;
					this.ready = true;
				});
			}
		});
	}

	/**
	 * Start/stop opening crawling.
	 */
	public toggleCrawl() {
		this.playCrawl = !this.playCrawl;
	}
}
