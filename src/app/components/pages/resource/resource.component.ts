import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { IResource } from 'src/app/types/resource.interface';

@Component({
	selector: 'app-resource',
	templateUrl: './resource.component.html',
	styleUrls: ['./resource.component.scss']
})
export class ResourceComponent implements OnInit {

	// The name of the displayed resource (people, planets, etc...)
	resource: string;

	// The current resource page, from route parameters.
	page: number;

	// The model for this component's view.
	data: IResource = null;

	// A state flag that is set to true when data is available.
	ready: boolean = false;

	/**
	 * Constructor
	 * @param {DataService} dataService Service to fetch the data for this component's model.
	 * @param {ActivatedRoute} route Angular service to manipulate the current route.
	 */
	constructor(public dataService: DataService, public route: ActivatedRoute) { }

	/**
	 * Called on component initialization. Get the resource data based
	 * on route parameters.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params.resource && params.page) {
				this.ready = false;
				this.resource = params.resource;
				this.page = params.page;
				this.dataService.getResource(params.resource, params.page).subscribe(result => {
					this.data = result;
					this.ready = true;
				}, error => {
					this.data = null;
					this.ready = true;
				});
			}
		});
	}

	/**
	 * Return the urls of the items to display in the item-list component.
	 * @returns Array<string>
	 */
	getItemsUrls(): Array<string> {
		if (!this.data) return [];
		return this.data.results.map(r => r.url);
	}
}
