import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourceComponent } from './resource.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ResourcePaginationComponent } from '../../ui/resource-pagination/resource-pagination.component';
import { ItemListComponent } from '../../ui/item-list/item-list.component';
import { ItemComponent } from '../../ui/item/item.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ResourceComponent', () => {
	let component: ResourceComponent;
	let fixture: ComponentFixture<ResourceComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule,
				HttpClientTestingModule
			],
			declarations: [
				ResourceComponent,
				ResourcePaginationComponent,
				ItemListComponent,
				ItemComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ResourceComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
