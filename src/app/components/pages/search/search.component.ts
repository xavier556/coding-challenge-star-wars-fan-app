import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute } from '@angular/router';
import { IResource } from 'src/app/types/resource.interface';
import { EResource } from 'src/app/types/resource.enum';
import { IItem } from 'src/app/types/item.interface';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

	// The search term, passed as route parameters.
	term: string = '';

	// The model for this component's view.
	data: Array<IResource> = null;

	// A state flag that is set to true when data is available.
	ready: boolean = true;

	/**
	 * Constructor
	 * @param {DataService} search Service to fetch the data for this component's model.
	 * @param {ActivatedRoute} route Angular service to manipulate the current route.
	 */
	constructor(public search: DataService, public route: ActivatedRoute) { }

	/**
	 * Called on component initialization. Get the result data based
	 * on route parameters.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params.term) {
				this.ready = false;
				this.term = params.term;
				this.search.searchTerm(params.term).subscribe(result => {
					this.data = result.length ? result : null;
					this.ready = true;
				}, error => {
					this.data = null;
					this.ready = true;
				});
			}
		});
	}

	getItemsUrls(resource: EResource): Array<string> {
		const data = this.data.filter(d => d.resourceName === resource);
		if (!data.length) return [];
		return data[0].results.map((r: IItem) => r.url);
	}
}
