import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchComponent } from './search.component';
import { FormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ItemComponent } from '../../ui/item/item.component';
import { RouterTestingModule } from '@angular/router/testing';
import { SearchBarComponent } from '../../ui/search-bar/search-bar.component';
import { ItemListComponent } from '../../ui/item-list/item-list.component';

describe('SearchComponent', () => {
	let component: SearchComponent;
	let fixture: ComponentFixture<SearchComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				FormsModule,
				RouterTestingModule,
				HttpClientTestingModule
			],
			declarations: [
				SearchComponent,
				SearchBarComponent,
				ItemListComponent,
				ItemComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(SearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
