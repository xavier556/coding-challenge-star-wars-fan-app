import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	// Model for the value of the username input.
	username: string;

	/**
	 * Constructor.
	 * @param auth 
	 */
	constructor(public auth: AuthService, public router: Router) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		// Redirect to home if already logged in.
		if (this.auth.isAuthenticated()) {
			this.router.navigate(['home']);
		}
	}

	/**
	 * Call the auth service with provided username.
	 * The redirection is handled by the service.
	 */
	login() {
		if (this.username) {
			this.auth.login(this.username);
		}
	}

	/**
	 * Login if the 'enter' key is pressed.
	 * @param event 
	 */
	onKeyDown(event) {
		if (event.key === 'Enter') {
			this.login();
		}
	}
}
