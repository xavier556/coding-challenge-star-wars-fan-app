import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EResource } from 'src/app/types/resource.enum';
import { IItem } from 'src/app/types/item.interface';

@Component({
	selector: 'app-vehicle',
	templateUrl: './vehicle.component.html',
	styleUrls: ['./vehicle.component.scss']
})
export class VehicleComponent implements OnInit {

	// The model for this component's view.
	public item: IItem = null;

	// A state flag that is set to true when data is available.
	ready: boolean = false;

	/**
	 * Constructor
	 * @param route 
	 * @param data 
	 */
	constructor(public route: ActivatedRoute, public data: DataService) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params.id) {
				this.ready = false;
				this.data.getItem(EResource.vehicles, params.id).subscribe(res => {
					console.log(res);
					this.item = res;
					this.ready = true;
				}, error => {
					this.item = null;
					this.ready = true;
				});
			}
		});
	}
}
