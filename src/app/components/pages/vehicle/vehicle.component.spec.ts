import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleComponent } from './vehicle.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { ItemComponent } from '../../ui/item/item.component';
import { ItemListComponent } from '../../ui/item-list/item-list.component';

describe('VehicleComponent', () => {
	let component: VehicleComponent;
	let fixture: ComponentFixture<VehicleComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule
			],
			declarations: [
				VehicleComponent,
				ItemListComponent,
				ItemComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(VehicleComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
