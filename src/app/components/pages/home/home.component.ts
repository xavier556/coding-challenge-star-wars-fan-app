import { Component, OnInit } from '@angular/core';
import { SwapiService } from 'src/app/services/swapi.service';
import { DataService } from 'src/app/services/data.service';


@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	// The model for this component's view.
	public data = {};

	// A state flag that is set to true when data is available.
	public ready: boolean = false;

	/**
	 * Constructor
	 * @param swapi 
	 * @param search 
	 */
	constructor(public swapi: SwapiService, public search: DataService) { }

	/**
	 * Called on component initialization. Get the list of available resources.
	 */
	ngOnInit() {
		this.ready = false;
		this.search.getResources().subscribe(res => {
			this.data = res;
			this.ready = true;
		}, error => {
			this.data = null;
			this.ready = true;
		});
	}

}
