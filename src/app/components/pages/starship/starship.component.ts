import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { EResource } from 'src/app/types/resource.enum';
import { IItem } from 'src/app/types/item.interface';

@Component({
	selector: 'app-starship',
	templateUrl: './starship.component.html',
	styleUrls: ['./starship.component.scss']
})
export class StarshipComponent implements OnInit {

	// The model for this component's view.
	public item: IItem = null;

	// A state flag that is set to true when data is available.
	ready: boolean = false;

	/**
	 * Constructor
	 * @param route 
	 * @param data 
	 */
	constructor(public route: ActivatedRoute, public data: DataService) { }

	/**
	 * Component initialization.
	 */
	ngOnInit() {
		this.route.params.subscribe(params => {
			if (params.id) {
				this.ready = false;
				this.data.getItem(EResource.starships, params.id).subscribe(res => {
					this.item = res;
					this.ready = true;
				}, error => {
					this.item = null;
					this.ready = true;
				});
			}
		});
	}
}
