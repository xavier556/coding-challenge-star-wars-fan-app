import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipComponent } from './starship.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { ItemListComponent } from '../../ui/item-list/item-list.component';
import { ItemComponent } from '../../ui/item/item.component';

describe('StarshipComponent', () => {
	let component: StarshipComponent;
	let fixture: ComponentFixture<StarshipComponent>;


	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule
			],
			declarations: [
				StarshipComponent,
				ItemListComponent,
				ItemComponent
			]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(StarshipComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
