import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SwapiService } from 'src/app/services/swapi.service';
import { DataService } from 'src/app/services/data.service';

@Component({
	selector: 'app-nav-bar',
	templateUrl: './nav-bar.component.html',
	styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

	// The model for the resource sub-menu.
	public resourceList = {};

	// A state flag that is set to true when data is available.
	public ready: boolean = false;

	/**
	 * Constructor.
	 * @param router 
	 * @param swapi 
	 * @param data 
	 */
	constructor(public router: Router, public swapi: SwapiService, public data: DataService) { }

	ngOnInit() {
		// Load the list of available resources to build the resource sub-menu dynamically.
		this.ready = false;
		this.data.getResources().subscribe(res => {
			this.resourceList = res;
			this.ready = true;
		});
	}

}
