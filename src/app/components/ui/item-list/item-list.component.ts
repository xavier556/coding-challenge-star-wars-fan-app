import { Component, OnInit, Input } from '@angular/core';
import { IItem } from 'src/app/types/item.interface';

@Component({
	selector: 'app-item-list',
	templateUrl: './item-list.component.html',
	styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

	@Input() items: Array<String>;

	@Input() title: string;

	constructor() { }

	ngOnInit() { }

}
