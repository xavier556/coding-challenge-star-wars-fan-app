import { Component, OnInit, Input } from '@angular/core';
import { IResource } from 'src/app/types/resource.interface';

@Component({
	selector: 'app-resource-pagination',
	templateUrl: './resource-pagination.component.html',
	styleUrls: ['./resource-pagination.component.scss']
})
export class ResourcePaginationComponent implements OnInit {

	@Input() data: IResource;

	constructor() { }

	ngOnInit() {
	}

	/**
	 * Return the route to the next page of the resource. We can determine if
	 * there is a next page by using the 'next' property of the resource data.
	 * @return {string}
	 */
	getNextPageRoute(): string {
		if (this.data.next) {
			return `/resource/${this.data.resourceName}/${this.data.resourcePage + 1}`;
		}
		return '';
	}

	/**
	 * Return the route to the previous page of the resource. We can determine if
	 * there is a previous page by using the 'previous' property of the resource data.
	 * @return {string}
	 */
	getPrevPageRoute(): string {
		if (this.data.previous) {
			return `/resource/${this.data.resourceName}/${this.data.resourcePage - 1}`;
		}
		return '';
	}

	/**
	 * Returns the route to the nth page of the resource.
	 * @param {number} i The page index.
	 * @return {string}
	 */
	getPageRoute(i: number): string {
		return `/resource/${this.data.resourceName}/${i}`;
	}

	/**
	 * 
	 */
	getVerbose(): string {
		return 'showing items ' + ((this.data.resourcePage - 1) * 10 + 1) + ' - ' + Math.min((this.data.resourcePage * 10), this.data.count) + ' of ' + this.data.count;
	}

	/**
	 * Create and return an array with n elements. This is used in the *ngFor
	 * directive to iterate n times.
	 * @param {number} n 
	 */
	counter(n: number): Array<any> {
		return new Array(Math.ceil(n / 10));
	}
}
