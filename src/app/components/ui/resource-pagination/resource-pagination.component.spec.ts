import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcePaginationComponent } from './resource-pagination.component';
import { RouterTestingModule } from '@angular/router/testing';
import { EResource } from 'src/app/types/resource.enum';
import { By } from '@angular/platform-browser';

describe('ResourcePaginationComponent', () => {
	let component: ResourcePaginationComponent;
	let fixture: ComponentFixture<ResourcePaginationComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				RouterTestingModule
			],
			declarations: [ResourcePaginationComponent]
		})
			.compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(ResourcePaginationComponent);
		component = fixture.componentInstance;
		component.data = {
			resourceName: EResource.films,
			resourcePage: 2,
			results: [],
			count: 89,
			next: '',
			previous: ''
		}
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});

	it('should have one link per 10 items (+ prev/next)', () => {
		const nbItems = [0, 1, 9, 10, 11, 56, 99];
		nbItems.forEach(i => {
			const expected = Math.ceil(i / 10) + 2;
			component.data.count = i;
			fixture.detectChanges();
			const listItems = fixture.nativeElement.querySelectorAll('li');
			expect(listItems.length).toEqual(expected);
		});
	});

	it('should disable previous link on first page', () => {
		component.data.resourcePage = 1;
		fixture.detectChanges();
		const prev: HTMLElement = fixture.nativeElement.querySelector('.page-previous');
		expect(prev.className.indexOf('disabled')).toBeGreaterThanOrEqual(0);
	});

	it('should disable next link on last page', () => {
		component.data.count = 91
		component.data.resourcePage = 10;
		fixture.detectChanges();
		const next: HTMLElement = fixture.nativeElement.querySelector('.page-next');
		expect(next.className.indexOf('disabled')).toBeGreaterThanOrEqual(0);
	});
});
