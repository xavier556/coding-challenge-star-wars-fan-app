import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'app-search-bar',
	templateUrl: './search-bar.component.html',
	styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

	// Model for the value of the search input.
	term: string = null;

	/**
	 * Constuctor.
	 */
	constructor() { }

	/**
	 * Component initialization.
	 */
	ngOnInit() { }
}
