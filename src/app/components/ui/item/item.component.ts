import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { IItem } from 'src/app/types/item.interface';
import { EResource } from 'src/app/types/resource.enum';

@Component({
	selector: 'app-item',
	templateUrl: './item.component.html',
	styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {

	@Input() url: string;

	data: IItem = null;

	route: string = '';

	ready: boolean = false;

	constructor(public dataService: DataService) { }

	ngOnInit() {
		if (this.url) {
			// Extract the resource and id from the api url.
			// Format is https://swapi.co/api/:resource/:id
			const parts = this.url.split('/');
			const resource: EResource = EResource[parts[4]];
			this.route = '/item/' + resource + '/' + parts[5];

			// Get the item from the data service.
			this.dataService.getItem(resource, parts[5]).subscribe(res => {
				this.data = res;
				this.ready = true;
			}, error => {
				this.data = null;
				this.ready = true;
			});
		}
	}

}
