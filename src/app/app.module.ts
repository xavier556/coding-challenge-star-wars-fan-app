import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/pages/home/home.component';
import { FrameComponent } from './components/frame/frame.component';
import { SearchComponent } from './components/pages/search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PeopleComponent } from './components/pages/people/people.component';
import { ItemComponent } from './components/ui/item/item.component';
import { SearchBarComponent } from './components/ui/search-bar/search-bar.component';
import { StarshipComponent } from './components/pages/starship/starship.component';
import { PlanetComponent } from './components/pages/planet/planet.component';
import { SpeciesComponent } from './components/pages/species/species.component';
import { VehicleComponent } from './components/pages/vehicle/vehicle.component';
import { FilmComponent } from './components/pages/film/film.component';
import { ResourceComponent } from './components/pages/resource/resource.component';
import { NavBarComponent } from './components/ui/nav-bar/nav-bar.component';
import { LoginComponent } from './components/pages/login/login.component';
import { ResourcePaginationComponent } from './components/ui/resource-pagination/resource-pagination.component';
import { ItemListComponent } from './components/ui/item-list/item-list.component';
import { CacheExplorerComponent } from './components/dev/cache-explorer/cache-explorer.component';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		HomeComponent,
		FrameComponent,
		SearchComponent,
		PeopleComponent,
		ItemComponent,
		SearchBarComponent,
		StarshipComponent,
		PlanetComponent,
		SpeciesComponent,
		VehicleComponent,
		FilmComponent,
		ResourceComponent,
		NavBarComponent,
		ResourcePaginationComponent,
		ItemListComponent,
		CacheExplorerComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		FormsModule
	],
	providers: [

	],
	bootstrap: [AppComponent]
})
export class AppModule { }
