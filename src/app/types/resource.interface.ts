import { IItem } from './item.interface';
import { EResource } from './resource.enum';

export interface IResource {
	resourceName: EResource,
	resourcePage: number,
	count: number,
	next: string,
	previous: string,
	results: Array<IItem>
}