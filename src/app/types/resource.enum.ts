export enum EResource {
	films = 'films',
	people = 'people',
	starships = 'starships',
	vehicles = 'vehicles',
	species = 'species',
	planets = 'planets'
}