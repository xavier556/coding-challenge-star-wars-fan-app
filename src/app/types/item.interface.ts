import { EResource } from './resource.enum';

export interface IItem {
	name: string,
	title: string,
	url: string,
	itemName: string,
	itemResource: EResource,
	itemId: string,
	opening_crawl: string
}
