import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { FrameComponent } from './components/frame/frame.component';
import { FormsModule } from '@angular/forms';
import { SearchBarComponent } from './components/ui/search-bar/search-bar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NavBarComponent } from './components/ui/nav-bar/nav-bar.component';

describe('AppComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				HttpClientTestingModule,
				RouterTestingModule,
				FormsModule
			],
			declarations: [
				AppComponent,
				FrameComponent,
				SearchBarComponent,
				NavBarComponent
			],
		}).compileComponents();
	}));

	it('should create the app', () => {
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	});
});
