import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/pages/home/home.component';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { SearchComponent } from './components/pages/search/search.component';
import { PeopleComponent } from './components/pages/people/people.component';
import { StarshipComponent } from './components/pages/starship/starship.component';
import { FilmComponent } from './components/pages/film/film.component';
import { VehicleComponent } from './components/pages/vehicle/vehicle.component';
import { PlanetComponent } from './components/pages/planet/planet.component';
import { ResourceComponent } from './components/pages/resource/resource.component';
import { SpeciesComponent } from './components/pages/species/species.component';
import { LoginComponent } from './components/pages/login/login.component';
import { CacheExplorerComponent } from './components/dev/cache-explorer/cache-explorer.component';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent,
	},
	{
		path: 'home',
		component: HomeComponent,
		canActivate: [AuthGuardService]
	},
	{
		path: 'search',
		canActivate: [AuthGuardService],
		children: [
			{ path: '', component: SearchComponent },
			{ path: ':term', component: SearchComponent }
		]
	},
	{
		path: 'resource/:resource',
		canActivate: [AuthGuardService],
		children: [
			{ path: '', redirectTo: '1', pathMatch: 'full' },
			{ path: ':page', component: ResourceComponent }
		]
	},
	{
		path: 'item',
		canActivate: [AuthGuardService],
		children: [
			{ path: '', redirectTo: '/home', pathMatch: 'full' },
			{ path: 'films/:id', component: FilmComponent },
			{ path: 'people/:id', component: PeopleComponent },
			{ path: 'planets/:id', component: PlanetComponent },
			{ path: 'species/:id', component: SpeciesComponent },
			{ path: 'starships/:id', component: StarshipComponent },
			{ path: 'vehicles/:id', component: VehicleComponent }
		]
	},
	{
		path: 'cache',
		component: CacheExplorerComponent,
		canActivate: [AuthGuardService]
	},
	{
		path: '',
		redirectTo: '/home',
		pathMatch: 'full'
	},
	{
		path: '**',
		component: HomeComponent,
		canActivate: [AuthGuardService]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {
		scrollPositionRestoration: 'top'
	})],
	exports: [RouterModule]
})
export class AppRoutingModule { }
